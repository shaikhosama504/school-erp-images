### Introduction

- Change the `frappe` and `erpnext` versions in `base_versions.json` to use them as base. These values correspond to tags and branch names on the github frappe and erpnext repo. e.g. `v14.0.1`
- Change `ci/clone-apps.sh` script to clone your private and public apps. Read comments in the file to update it as per need. This repo will install following apps:
  - https://github.com/resilient-tech/india-compliance
- Change `images/backend.Dockerfile` to copy and install required apps with `install-app`.

### Manually Build images

Execute from root of app repo

Clone,

```shell
./ci/clone-apps.sh
```

Set environment variables,

- `FRAPPE_VERSION` set to use frappe version during building images. Default is `version-13`.
- `ERPNEXT_VERSION` set to use erpnext version during building images. Default is `version-13`.
- `VERSION` set the tag version. Default is `latest`.
- `REGISTRY_NAME` set the registry name. Default is `custom_app`.
- `BACKEND_IMAGE_NAME` set worker image name. Default is `custom_worker`.
- `FRONTEND_IMAGE_NAME` set nginx image name. Default is `custom_nginx`.

Build,

```shell
FRAPPE_VERSION=$(jq -r .frappe base_versions.json) ERPNEXT_VERSION=$(jq -r .erpnext base_versions.json) VERSION=$(cat version.txt) docker buildx bake --load
```

Note:

- Use `docker buildx bake --load` to load images for usage with docker.
- Use `docker buildx bake --push` to push images to registry.
- Use `docker buildx bake --help` for more information.
- Change version in `version.txt` to build tagged images from the changed version.
